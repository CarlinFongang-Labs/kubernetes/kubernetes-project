# Kubernetes | Déploiement d'une BD MySQL et Wordpress (PV, PVC, et deployment)
_______

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Ce laboratoire vise à effectuer un déploiement de Wordpress sur un cluster Kubernetes.

## Objectifs

1. **Création d'un déploiement MySQL** : Nous mettrons en place un déploiement MySQL avec un seul réplica pour gérer notre base de données.

2. **Exposition de MySQL** : Un service de type ClusterIP sera créé pour exposer nos pods MySQL, permettant ainsi à d'autres services du cluster de communiquer avec la base de données.

3. **Déploiement de WordPress** : Nous configurerons un déploiement WordPress avec les variables d'environnement nécessaires pour se connecter à la base de données MySQL.

4. **Stockage des données WordPress** : Le déploiement WordPress sera configuré pour stocker les données dans un volume monté sur le répertoire `/data` d’un des nœuds du cluster, assurant la persistance des données.

5. **Exposition de WordPress** : Nous créerons un service de type NodePort pour exposer l'interface de WordPress à l'extérieur, rendant le site accessible sur internet.


## 1. **Prérequis** : Liste des exigences matérielles et logicielles.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu 20.04 Focal Fossa LTS, grace au provider AWS, à partir delaquelle nous effectuerons toutes nos opérations.

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws) (recommandé)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)


## 2. Création d'un cluster

Consultez le document [Installer kubeadm](https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git)


Voici une structure détaillée pour la documentation technique sur l'installation de WordPress via Kubernetes :

## 2. Création d'un namespace dédié au déploiement de wordpress

Afin d'isoler notre travail, nous allons crée un namespace que nous allons nommé **"wordpress-prod"**

````bash
nano namespace.yml
````

````yaml
apiVersion: v1
kind: Namespace
metadata:
  name: wordpress-prod
````

````bash
 kubectl apply -f namespace.yml
````

>![alt text](img/image.png)
*Le namespace à été crée*


## 3. Création du Persistent Volume

````bash
nano mysql-pv.yml
````

Description du manfiest

````yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mysql-pv
  labels:
    type: local
spec:
  capacity:
    storage: 3Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: standard
  hostPath:
    path: /data/mysql-pv
````

````bash
kubectl apply -f mysql-pv.yml
````

>![alt text](img/image-1.png)
*Le Persistent Volume (PV) a été crée*


## 3. **Création du déploiement MySQL**

### 3.1. Manifeste PersistentVolumeClaim

````bash
nano mysql-pvc.yml
````

````yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pvc
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 2Gi
  storageClassName: standard
````

````bash
kubectl apply -f mysql-pvc.yml
````

>![alt text](img/image-2.png)


### 3.2. Détails du manifeste de déploiement.

````bash
nano mysql-deployment.yml
````

Contenu du manifest

````yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress-mysql
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
      - name: mysql
        image: mysql:latest
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: "password"
        - name: MYSQL_DATABASE
          value: "db-acd"
        - name: MYSQL_USER
          value: "acd"
        - name: MYSQL_PASSWORD
          value: "devops-acd"
        ports:
        - containerPort: 3306
        volumeMounts:
        - name: mysql-data
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-data
        persistentVolumeClaim:
          claimName: mysql-pvc
````

````bash
kubectl apply -f mysql-deployment.yml 
````

Nous pouvons vérifier les pods en cours d'exécution : 

````bash
kubectl get pod
````
>![alt text](img/image-3.png)
*Pod mysql en cours d'exécution*

L'on peut vérifier également les détails du pod en cours d'exécution et constater et constater qu'il utiliser bien le PVC déclaré un précédement

````bash
 kubectl describe pods wordpress-mysql-75b599b99-9d756
````

>![alt text](img/image-4.png)

### 3.3. Création du service ClusterIP pour MySQL.
Pour permettre à d'autres pods de communiquer avec MySQL, nous exposerons le déploiement via un service de type ClusterIP.

````bash
nano sv-mysql.yml
````

````yaml
apiVersion: v1
kind: Service
metadata:
  name: sv-mysql
spec:
  type: ClusterIP
  ports:
  - port: 3306
  selector:
    app: mysql
````

````bash
kubectl apply -f sv-mysql.yml
````

>![alt text](img/image-5.png)
*ClusterIP en cours d'exécution*


En entrant la commande ci-dessous, on peut virifier que le ClusterIP est bien exécuté et associé aux pod dont le label est bien **"app=mysql"**

````bash
kubectl describe svc sv-mysql
````

>![alt text](img/image-6.png)


## 4. **Installation de WordPress**

 ### 4.1. Création du Persistent Volume

````bash
nano wordpress-pv.yml
````

````yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: wordpress-pv
  labels:
    type: local
spec:
  capacity:
    storage: 3Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: standard
  hostPath:
    path: /data/wordpress-pv
````


````bash
kubectl apply -f wordpress-pv.yml
````

>![alt text](img/image-7.png)


  ### 4.2. Manifeste PersistentVolumeClaim pour WordPress

````bash
nano wordpress-pvc.yml
````

````yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: wordpress-pvc
spec:
  storageClassName: standard
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
````

````bash
kubectl apply -f wordpress-pvc.yml
````

>![alt text](img/image-8.png)

   ### 4.3. Manifest de déploiement de WordPress.

````bash
nano wordpress-deployment.yml
````

````yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress
spec:
  replicas: 1
  selector:
    matchLabels:
      app: wordpress
  template:
    metadata:
      labels:
        app: wordpress
    spec:
      containers:
      - name: wordpress
        image: wordpress:latest
        env:
        - name: WORDPRESS_DB_HOST
          value: "sv-mysql"  # Doit correspondre au nom du service qui expose MySQL
        - name: WORDPRESS_DB_USER
          value: "acd"              # Doit correspondre à MYSQL_USER
        - name: WORDPRESS_DB_PASSWORD
          value: "devops-acd"       # Doit correspondre à MYSQL_PASSWORD
        - name: WORDPRESS_DB_NAME
          value: "db-acd"           # Doit correspondre à MYSQL_DATABASE
        ports:
        - containerPort: 80
        volumeMounts:
        - name: wordpress-storage
          mountPath: /var/www/html
      volumes:
      - name: wordpress-storage
        persistentVolumeClaim:
          claimName: wordpress-pvc
````

````bash
 kubectl get deploy
````

>![alt text](img/image-9.png)

On peut également vérifier à quel PVC le pod wordpress est rataché 
````bash
kubectl get pod
````

ensuite 

````bash
kubectl describe pods wordpress-6bcf6b499b-92b2z
````

>![alt text](img/image-10.png)


  ### 4.3. Exposition de wordpress

````bash
nano sv-wordpress.yml
````

````yaml
apiVersion: v1
kind: Service
metadata:
  name: wordpress
spec:
  type: NodePort
  ports:
  - port: 80
    targetPort: 80
    nodePort: 30000
  selector:
    app: wordpress
````

````bash
kubectl apply -f sv-wordpress.yml
````

````bash
kubectl get services
````

>![alt text](img/image-11.png)


5. **Vérification et Tests**

En entrant l'ip d'un des nodes suivi du port 30000 dans le navigateur, on accède à la page de configuration de wordpress

>![alt text](img/image-12.png)



## Documentation 

### [Volumes Persistants](https://kubernetes.io/fr/docs/concepts/storage/persistent-volumes/)

### [Installer kubeadm sur ubuntu](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

### [Creating a Single Control-Plane Cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)
